private void ShellSort(long[] inputArray)
{
    long j, temp = 0;
    int increment = (inputArray.Length) / 2;
    while (increment > 0)
    {
        for (int index = 0; index < inputArray.Length; index++)
        {
            j = index;
            temp = inputArray[index];
            while ((j >= increment) && inputArray[j - increment] > temp)
            {
                inputArray[j] = inputArray[j - increment];
                j = j - increment;
            }
            inputArray[j] = temp;
        }
        if (increment / 2 != 0)
            increment = increment / 2;
        else if (increment == 1)
            increment = 0;
        else
            increment = 1;
    }
}